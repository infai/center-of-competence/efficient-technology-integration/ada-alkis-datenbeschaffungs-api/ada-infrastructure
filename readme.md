# CKAN deployment for ADA project

This repository is built to be deployed with podman (rootless or rootful) or docker managed infrastructure. Examples are written with podman and podman-compose, which are exchangeable with docker and docker-compose.

Tested with these versions:
* podman>=4.0.0
* podman-compose>=1.0.0

Should work with:
* docker>=1.27.0
* docker-compose>=2.0.0

## Local deployment
1. `podman-compose -f compose.base.yaml -f compose.local.yaml pull`
2. `podman-compose -f compose.base.yaml -f compose.local.yaml build`
3. `podman-compose -f compose.base.yaml -f compose.local.yaml up -d`

This will start ckan and map it to the host port 5000. The default admin-user has credentials admin:password.

## Staging deployment

Edit URLs, Mails and credential variables in compose.staging.yaml as needed. For podman it is needed to [activate the podman socket](https://docs.podman.io/en/latest/markdown/podman-system-service.1.html) in order for the reverse-proxy to work (review its volumes to choose the correct socket).

1. `podman-compose -f compose.base.yaml -f compose.staging.yaml pull`
2. `podman-compose -f compose.base.yaml -f compose.staging.yaml build`
3. `podman-compose -f compose.base.yaml -f compose.staging.yaml up -d`

This will start ckan, setup a reverse-proxy and request HTTPS certificates for the specified domain. Ports 80 and 443 will be mapped to the host.

## Acknowledgements
This repository was built with information from:
* [Minipod](https://gitlab.com/infai/minipod)
* [CKAN](https://github.com/ckan/ckan)
* [ckanext-harvest](https://github.com/ckan/ckanext-harvest)
* [ckanext-dcat](https://github.com/ckan/ckanext-dcat)
